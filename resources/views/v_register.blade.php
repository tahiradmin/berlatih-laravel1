<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form method="POST" action="/welcome">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="2">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="0">Indonesian</option>
            <option value="1">Singaporean</option>
            <option value="2">Malaysian</option>
            <option value="3">Australian</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" value="0">Bahasia Indonesia <br>
        <input type="checkbox" name="bahasa" value="1">English <br>
        <input type="checkbox" name="bahasa" value="2">Other <br>
        <br>
        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea>
        <br>

        <input type="submit" value="Sign Up">

    </form>  
</body>
</html>