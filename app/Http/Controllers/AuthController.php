<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AuthController extends Controller
{
    public function register(){
        return view('v_register');
    }

    public function welcome(Request $request){
    
        $firstname = $request->firstname;
        $lastname = $request->lastname;
      
        //dd('$firstname');
       return view('v_welcome', compact('firstname','lastname'));
       
       
    }

}
